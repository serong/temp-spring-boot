package com.ong.sectorsdemo.dtos;

import com.ong.sectorsdemo.models.UserForm;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;
import java.util.stream.Collectors;

public class UserFormDTO {

    private Long id;

    @NotBlank(message = "User name is required.")
    private String name;
    @NotEmpty(message = "At least one sector must be chosen.")
    private List<SectorDTO> sectors;
    @AssertTrue(message = "Agreement needs to be accepted.")
    private boolean agreementAccepted;

    public UserFormDTO() {}

    public UserFormDTO(Long id, String name, List<SectorDTO> sectors, boolean agreementAccepted) {
        this.id = id;
        this.name = name;
        this.sectors = sectors;
        this.agreementAccepted = agreementAccepted;
    }

    public UserFormDTO(String name, List<SectorDTO> sectors, boolean agreementAccepted) {
        this.name = name;
        this.sectors = sectors;
        this.agreementAccepted = agreementAccepted;
    }

    public static UserFormDTO buildFromModel(UserForm userFormModel) {
        List<SectorDTO> sectors = userFormModel.getSectors().stream()
                                               .map(sector -> SectorDTO.build(sector))
                                               .collect(Collectors.toList());
        return new UserFormDTO(
                userFormModel.getId(), userFormModel.getName(), sectors, userFormModel.isAgreementAccepted()
        );
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<SectorDTO> getSectors() {
        return sectors;
    }

    public void setSectors(List<SectorDTO> sectors) {
        this.sectors = sectors;
    }

    public boolean isAgreementAccepted() {
        return agreementAccepted;
    }

    public void setAgreementAccepted(boolean agreementAccepted) {
        this.agreementAccepted = agreementAccepted;
    }
}
