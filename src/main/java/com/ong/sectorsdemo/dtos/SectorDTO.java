package com.ong.sectorsdemo.dtos;

import com.ong.sectorsdemo.models.Sector;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class SectorDTO {
    private Long id;
    private String name;
    private Integer depth;

    public SectorDTO() {}

    public SectorDTO(Long id, String name, Integer depth) {
        this.id = id;
        this.name = name;
        this.depth = depth;
    }

    private SectorDTO(Sector sectorModel) {
        this.depth = sectorModel.getDepth();
        this.name = sectorModel.getName();
        this.id = sectorModel.getId();
    }

    public static SectorDTO build(Sector sectorModel) {
        return new SectorDTO(sectorModel);
    }
}
