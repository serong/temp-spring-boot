package com.ong.sectorsdemo.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "User form could not be saved.")
public class FormSaveException extends RuntimeException {

    public FormSaveException() {
    }

    public FormSaveException(String message) {
        super(message);
    }

    public FormSaveException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public FormSaveException(Throwable throwable) {
        super(throwable);
    }
}










