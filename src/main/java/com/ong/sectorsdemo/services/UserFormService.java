package com.ong.sectorsdemo.services;

import com.ong.sectorsdemo.dtos.SectorDTO;
import com.ong.sectorsdemo.dtos.UserFormDTO;
import com.ong.sectorsdemo.exceptions.FormSaveException;
import com.ong.sectorsdemo.models.Sector;
import com.ong.sectorsdemo.models.UserForm;
import com.ong.sectorsdemo.repositories.UserFormRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserFormService {

    @Autowired
    UserFormRepository userFormRepository;

    @Autowired
    SectorService sectorService;

    public UserFormDTO update(Long id, String name, List<SectorDTO> sectorDTOS, boolean accepted) {
        UserForm form = userFormRepository.findById(id).orElseThrow(FormSaveException::new);
        List<Sector> sectors = sectorService.mapToSector(sectorDTOS);

        form.setName(name);
        form.setSectors(sectors);
        form.setAgreementAccepted(accepted);

        form = userFormRepository.save(form);

        return UserFormDTO.buildFromModel(form);
    }

    public UserFormDTO save(String name, List<SectorDTO> sectors, boolean accepted) {

        List<Sector> fetchedSectors = sectorService.mapToSector(sectors);
        UserForm form = new UserForm.Builder(name)
                .sectors(fetchedSectors)
                .accepted(accepted)
                .build();

        UserForm saved = userFormRepository.save(form);

        if (saved != null) {
            return UserFormDTO.buildFromModel(saved);
        }

        throw new FormSaveException("Could not be saved");
    }

}
