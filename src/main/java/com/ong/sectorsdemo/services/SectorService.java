package com.ong.sectorsdemo.services;

import com.ong.sectorsdemo.dtos.SectorDTO;
import com.ong.sectorsdemo.models.Sector;
import com.ong.sectorsdemo.repositories.SectorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SectorService {

    @Autowired
    private SectorRepository sectorRepository;

    public List<SectorDTO> getAllSectors() {
        return sectorRepository.getAllWithDepth()
                               .stream()
                               .filter(sector -> !sector.getName().equalsIgnoreCase("ROOT"))
                               .map(sector -> SectorDTO.build(sector))
                               .collect(Collectors.toList());
    }

    public List<Sector> mapToSector(List<SectorDTO> sectorDTOS) {
        return sectorRepository.getAllWithDepthByID(
                sectorDTOS.stream()
                          .map(sector -> sector.getId())
                          .collect(Collectors.toList()));
    }
}
