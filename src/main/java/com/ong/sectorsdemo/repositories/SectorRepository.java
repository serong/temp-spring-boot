package com.ong.sectorsdemo.repositories;

import com.ong.sectorsdemo.models.Sector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SectorRepository extends JpaRepository<Sector, Long> {
    String allWithDepth = "SELECT S.id, S.name, S.border_left, S.border_right, (COUNT(parent.name) - 1) AS depth " +
                          "FROM sector AS S, sector AS parent " +
                          "WHERE S.border_left BETWEEN parent.border_left AND parent.border_right " +
                          "GROUP BY S.id ORDER BY S.border_left;";

    String allByIDWithDepth = "SELECT S.id, S.name, S.border_left, S.border_right, (COUNT(parent.name) - 1) AS depth " +
                              "FROM sector AS S, sector AS parent " +
                              "WHERE S.border_left BETWEEN parent.border_left AND parent.border_right " +
                              "AND S.id IN :ids " +
                              "GROUP BY S.id ORDER BY S.border_left;";

    @Query(value = allWithDepth, nativeQuery = true)
    List<Sector> getAllWithDepth();

    @Query(value = allByIDWithDepth, nativeQuery = true)
    List<Sector> getAllWithDepthByID(@Param("ids") List<Long> sectorIds);
}


