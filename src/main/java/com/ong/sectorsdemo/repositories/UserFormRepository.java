package com.ong.sectorsdemo.repositories;

import com.ong.sectorsdemo.models.UserForm;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserFormRepository extends CrudRepository<UserForm, Long> {

    List<UserForm> findAll();

}
