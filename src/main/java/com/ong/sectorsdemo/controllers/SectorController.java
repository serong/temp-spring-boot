package com.ong.sectorsdemo.controllers;

import com.ong.sectorsdemo.dtos.SectorDTO;
import com.ong.sectorsdemo.services.SectorService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@Api(value = "Sectors", tags = {"sectors"})
@CrossOrigin(origins = "*")
public class SectorController {

    @Autowired
    private SectorService sectorService;

    @GetMapping("/api/sector/all")
    public ResponseEntity<List<SectorDTO>> getAllSectors() {
        List<SectorDTO> sectors = sectorService.getAllSectors();

        return ResponseEntity.ok(sectors);
    }
}
