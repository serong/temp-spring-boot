package com.ong.sectorsdemo.controllers;

import com.ong.sectorsdemo.dtos.UserFormDTO;
import com.ong.sectorsdemo.exceptions.FormSaveException;
import com.ong.sectorsdemo.services.UserFormService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Api(value = "User Form", tags = {"user-form"})
@CrossOrigin(origins = "*")
public class UserFormController {

    @Autowired
    UserFormService userFormService;

    @PostMapping("/api/user-form/")
    public ResponseEntity<UserFormDTO> save(@Valid @RequestBody UserFormDTO userFormDTO) {
        UserFormDTO response = userFormService.save(
                userFormDTO.getName(),
                userFormDTO.getSectors(),
                userFormDTO.isAgreementAccepted());

        return ResponseEntity.ok(response);
    }

    @PatchMapping("/api/user-form/{id}")
    public ResponseEntity<UserFormDTO> update(@Valid @RequestBody UserFormDTO userFormDTO, @PathVariable Long id) {
        UserFormDTO userForm = userFormService.update(
                id,
                userFormDTO.getName(),
                userFormDTO.getSectors(),
                userFormDTO.isAgreementAccepted());

        return ResponseEntity.ok(userForm);
    }

    @ExceptionHandler(FormSaveException.class)
    public ResponseEntity handleFormNotSaved(Exception ex) {
        return ResponseEntity.status(404).body("Update/save operation failed.");
    }

}
