package com.ong.sectorsdemo.helper;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * Helper class to help with parsing list of sectors from a text
 * into a format that can be turned into Nested Set model in the
 * database.
 *
 * Didn't want to populate the DB by hand. :)
 */
@Getter
@Setter
public class SectorNode {

    private int borderLeft;
    private int borderRight;
    private int depth;

    private String name;
    private SectorNode parent;
    private List<SectorNode> children;

    public SectorNode(String name) {
        this.name = name;
        this.children = new ArrayList<>();
    }

    public SectorNode(String name, int depth) {
        this.name = name;
        this.depth = depth;
        this.children = new ArrayList<>();
    }

    public SectorNode(int borderLeft, int borderRight, int depth, String name, SectorNode parent) {
        this.borderLeft = borderLeft;
        this.borderRight = borderRight;
        this.depth = depth;
        this.parent = parent;
        this.name = name;
        this.children = new ArrayList<>();
    }

    public boolean hasChildren() {
        return this.children.size() > 0;
    }

    public void registerAsChild(SectorNode child) {
        this.children.add(child);
        child.setParent(this);
        child.setDepth(this.depth + 1);
    }

    public void registerAsSibling(SectorNode sibling) {
        this.parent.registerAsChild(sibling);
    }

    // Walk up the tree and find a common ancestor using a given relative node.
    public SectorNode findCommonAncestor(SectorNode relative) {
        while (relative.getDepth() > this.depth) {
            relative = relative.getParent();
        }
        return relative;
    }
}
