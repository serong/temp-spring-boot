package com.ong.sectorsdemo.helper;

import java.io.*;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

public class SectorNodeParser {
    public static void main(String[] args) throws URISyntaxException, IOException {

        SectorNode parsedRoot = parseTextToNodes("database-sectors.txt");
        walkAndUpdateBorders(parsedRoot);

        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("database-sectors.sql"));
        bufferedWriter.write("INSERT INTO sector (name, border_left, border_right) VALUES \n");

        createSQL(parsedRoot, bufferedWriter);
        bufferedWriter.write(";");
        bufferedWriter.close();
    }

    /**
     * Create the parent/children relationship.
     */
    public static SectorNode parseTextToNodes(String filename) throws FileNotFoundException {

        BufferedReader reader = new BufferedReader(new FileReader(filename));
        List<String> lines = reader.lines().collect(Collectors.toList());

        SectorNode rootSector = new SectorNode(1, ((lines.size() + 1) * 2), 0, "ROOT", null);
        SectorNode prevNode = rootSector;

        for (String line : lines) {
            Integer depth = getLevel(line);
            String name = getName(line);

            SectorNode currentNode = new SectorNode(name, depth);

            if (currentNode.getDepth() > prevNode.getDepth()) {
                prevNode.registerAsChild(currentNode);
            }

            if (currentNode.getDepth() == prevNode.getDepth()) {
                prevNode.registerAsSibling(currentNode);
            }

            if (currentNode.getDepth() < prevNode.getDepth()) {
                currentNode.findCommonAncestor(prevNode).registerAsSibling(currentNode);
            }

            prevNode = currentNode;
        }

        return rootSector;
    }

    /**
     * Walk the tree and update borderLeft and borderRight.
     */
    public static void walkAndUpdateBorders(SectorNode node) {

        if (!node.hasChildren()) {
            node.setBorderRight(node.getBorderLeft() + 1);
        } else {
            for (int i = 0; i < node.getChildren().size(); i++) {

                SectorNode currentSector = node.getChildren().get(i);

                if (i == 0) {
                    currentSector.setBorderLeft(node.getBorderLeft() + 1);
                    walkAndUpdateBorders(currentSector);
                } else {
                    currentSector.setBorderLeft(node.getChildren().get(i - 1).getBorderRight() + 1);
                    currentSector.setBorderRight(currentSector.getBorderLeft() + 1);

                    walkAndUpdateBorders(currentSector);

                    if (i == node.getChildren().size() - 1) {
                        node.setBorderRight(currentSector.getBorderRight() + 1);
                    }
                }
            }
        }
    }

    public static void createSQL(SectorNode root, BufferedWriter bufferedWriter) throws IOException {
        bufferedWriter.write("\t ('" + root.getName().replace("'", "").trim() +
                                     "', " + root.getBorderLeft() + ", " + root.getBorderRight() + "), \n");

        for (SectorNode child : root.getChildren()) {
            createSQL(child, bufferedWriter);
        }
    }


    public static Integer getLevel(String line) {
        return line.split("-").length - 1;
    }

    public static String getName(String line) {
        String[] split = line.split("- ");

        return split[split.length - 1];
    }
}
