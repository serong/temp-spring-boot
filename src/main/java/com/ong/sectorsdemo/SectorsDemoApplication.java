package com.ong.sectorsdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SectorsDemoApplication {


	public static void main(String[] args) {
		SpringApplication.run(SectorsDemoApplication.class, args);

	}

}
