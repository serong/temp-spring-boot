package com.ong.sectorsdemo.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter @Setter @NoArgsConstructor
public class UserForm {
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private boolean agreementAccepted;

    @ManyToMany(fetch = FetchType.EAGER)
    private List<Sector> sectors;

    public static class Builder {
        private Long id;
        private String name;
        private boolean agreementAccepted;
        private List<Sector> sectors;

        public Builder(String name) {
            this.name = name;
        }

        public Builder sectors(List<Sector> sectors) {
            this.sectors = sectors;
            return this;
        }

        public Builder accepted(boolean agreementAccepted) {
            this.agreementAccepted = agreementAccepted;
            return this;
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public UserForm build() {
            UserForm form = new UserForm();
            form.setName(this.name);
            form.setId(this.id);
            form.setSectors(this.sectors);
            form.setAgreementAccepted(this.agreementAccepted);

            return form;
        }
    }
}
