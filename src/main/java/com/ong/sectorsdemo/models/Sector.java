package com.ong.sectorsdemo.models;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Transient;

import javax.persistence.*;


/**
 * Sectors modelled as Nested Sets
 *
 * They are preferred when the hierarchy is more read-heavy.
 *
 * https://en.wikipedia.org/wiki/Nested_set_model
 */
@Entity
@Getter
@Setter
@NoArgsConstructor
public class Sector {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;
    private Integer borderLeft;
    private Integer borderRight;

    @Column(insertable = false)
    @Transient
    private Integer depth;

    public static class Builder {
        private Long id;
        private String name;
        private Integer borderLeft;
        private Integer borderRight;
        private Integer depth;

        public Builder(String name) {
            this.name = name;
        }

        public Builder borderLeft(Integer borderLeft) {
            this.borderLeft = borderLeft;
            return this;
        }

        public Builder borderRight(Integer borderRight) {
            this.borderRight = borderRight;
            return this;
        }

        public Builder depth(Integer depth) {
            this.depth = depth;
            return this;
        }

        public Builder id(Long id) {
            this.id = id;
            return this;
        }

        public Sector build() {
            Sector sector = new Sector();
            sector.setId(this.id);
            sector.setName(this.name);
            sector.setBorderLeft(this.borderLeft);
            sector.setBorderRight(this.borderRight);
            sector.setDepth(this.depth);

            return sector;
        }
    }
}
