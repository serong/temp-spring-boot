DELETE FROM sector;
INSERT INTO sector (id, name, border_left, border_right) VALUES
(1,'ROOT',1, 14),
(2, 'A!!', 2, 9),
(3, 'B', 10, 13),
(4, 'AA', 3, 4),
(5, 'AB', 5, 6),
(6, 'AC', 7, 8),
(7, 'BA', 11, 12);
