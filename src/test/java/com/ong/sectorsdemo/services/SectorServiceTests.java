package com.ong.sectorsdemo.services;

import com.ong.sectorsdemo.dtos.SectorDTO;
import com.ong.sectorsdemo.models.Sector;
import com.ong.sectorsdemo.repositories.SectorRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("test")
public class SectorServiceTests {
    @Autowired
    private SectorService sectorService;

    @MockBean
    SectorRepository mockSectorRepository;

    @Test
    public void getAllSectors_success() {
        List<Sector> mockSectors = Arrays.asList(
                new Sector.Builder("ROOT").borderLeft(1).borderRight(14).depth(0).id(1L).build(),
                new Sector.Builder("A").borderLeft(2).borderRight(9).depth(1).id(2L).build(),
                new Sector.Builder("AAA").borderLeft(3).borderRight(4).depth(2).id(3L).build(),
                new Sector.Builder("ABB").borderLeft(5).borderRight(6).depth(2).id(4L).build(),
                new Sector.Builder("ACC").borderLeft(7).borderRight(8).depth(2).id(5L).build(),
                new Sector.Builder("B").borderLeft(10).borderRight(13).depth(1).id(6L).build(),
                new Sector.Builder("BA").borderLeft(11).borderRight(12).depth(2).id(7L).build());

        when(mockSectorRepository.getAllWithDepth()).thenReturn(mockSectors);

        List<SectorDTO> sectorDTOList = sectorService.getAllSectors();

        Assert.assertEquals("ROOT element shouldn't be returned.",
                            sectorDTOList.get(0).getName(), "A");

        Assert.assertEquals("Correct depth should be returned.",
                            sectorDTOList.get(1).getDepth(), Integer.valueOf(2));
    }

    @Test
    public void mapToSector() {
        SectorDTO sectorOne = new SectorDTO(1L, "Name A", 1);
        SectorDTO sectorTwo = new SectorDTO(2L, "Name AB", 2);

        List<Sector> expectedSectors = sectorService.mapToSector(Arrays.asList(sectorOne, sectorTwo));

        verify(mockSectorRepository, times(1)).getAllWithDepthByID(Arrays.asList(1L, 2L));
    }
}
