package com.ong.sectorsdemo.services;

import com.ong.sectorsdemo.dtos.SectorDTO;
import com.ong.sectorsdemo.dtos.UserFormDTO;
import com.ong.sectorsdemo.exceptions.FormSaveException;
import com.ong.sectorsdemo.models.Sector;
import com.ong.sectorsdemo.models.UserForm;
import com.ong.sectorsdemo.repositories.SectorRepository;
import com.ong.sectorsdemo.repositories.UserFormRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("test")
public class UserFormServiceTests {
    @Autowired
    UserFormService userFormService;

    @MockBean
    UserFormRepository mockUserFormRepository;

    @MockBean
    SectorService mockSectorService;


    @Test
    public void save_success() {
        Sector mockPersistedSector = new Sector.Builder("Sector Name")
                .borderLeft(2).borderRight(4)
                .depth(null).id(3L).build();

        UserForm mockPersistedUserForm = new UserForm.Builder("Form Name")
                .accepted(true).sectors(Arrays.asList(mockPersistedSector))
                .id(1L).build();

        when(mockSectorService.mapToSector(any(List.class)))
                .thenReturn(Arrays.asList(mockPersistedSector));

        when(mockUserFormRepository.save(any(UserForm.class)))
                .thenReturn(mockPersistedUserForm);


        UserFormDTO response = userFormService.save("Name", new ArrayList<>(), true);

        Assert.assertNotEquals(null, response);
    }

    @Test(expected = FormSaveException.class)
    public void save_exception() {
        when(mockUserFormRepository.save(any(UserForm.class)))
                .thenReturn(null);

        userFormService.save("Name", new ArrayList<>(), true);
    }

}
