package com.ong.sectorsdemo.repositories;

import com.ong.sectorsdemo.models.Sector;
import com.ong.sectorsdemo.models.UserForm;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("test")
public class UserFormRepositoryTests {

    @Autowired
    UserFormRepository userFormRepository;

    @Autowired
    SectorRepository sectorRepository;

    @Before
    public void cleanUp() {
        userFormRepository.deleteAll();
    }

    @Test
    public void save_success() {
        Assert.assertEquals("Initial size should be 0", 0, userFormRepository.findAll().size());

        List<Sector> sectors = sectorRepository.getAllWithDepthByID(Arrays.asList(2L, 3L));
        UserForm saved = userFormRepository.save(new UserForm.Builder("NEW")
                                                         .sectors(sectors)
                                                         .accepted(true)
                                                         .build());

        Assert.assertEquals("Form should be added.",
                            1, userFormRepository.findAll().size());
    }


    @Test
    public void update_success() {
        List<Sector> sectors = sectorRepository.getAllWithDepthByID(Arrays.asList(2L, 3L));
        UserForm savedForm = userFormRepository.save(new UserForm.Builder("NAME a")
                                                             .sectors(sectors)
                                                             .accepted(false)
                                                             .build());

        savedForm.setName("Name B");
        savedForm = userFormRepository.save(savedForm);

        Assert.assertEquals("It should have updated import.sql",
                            "Name B", savedForm.getName());
    }
}
