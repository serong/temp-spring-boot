package com.ong.sectorsdemo.repositories;

import com.ong.sectorsdemo.models.Sector;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest()
@ActiveProfiles("test")
public class SectorRepositoryTests {
    @Autowired
    SectorRepository sectorRepository;

    @Test
    public void getAllWithDepth_success() {
        /**
         * Sector list as initialized in the DB:
         * ROOT, 0 (depth)
         * - A, 1
         * -- AA, 2
         * -- AB, 2
         * -- AC, 2
         * - B, 1
         * -- BA, 2
         */

        List<Sector> sectors = sectorRepository.getAllWithDepth();

        Assert.assertEquals(7, sectors.size());
        Assert.assertEquals("AB", sectors.get(3).getName());
        Assert.assertEquals(Integer.valueOf(2), sectors.get(3).getDepth());
    }

    @Test
    public void getAllWithDepthByID_success() {
        /**
         * Sector list as initialized in the DB:
         * ROOT, 0 (depth)
         * - A, 1
         * -- AA, 2
         * -- AB, 2
         * -- AC, 2
         * - B, 1
         * -- BA, 2
         */

        List<Sector> sectors = sectorRepository.getAllWithDepthByID(Arrays.asList(2L, 4L));

        Assert.assertEquals("2 records should be found.", 2, sectors.size());
        Assert.assertEquals("Correct depth should be fetched.", Integer.valueOf(2), sectors.get(1).getDepth());
    }
}
