package com.ong.sectorsdemo.integration;

import com.ong.sectorsdemo.controllers.UserFormController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest()
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class IntegrationTests {

    @Autowired
    UserFormController userFormController;

    @Autowired
    MockMvc mockMvc;

    @Test
    public void getSectors() throws Exception {
        String expectedJson = "{\"id\":2,\"name\":\"A!!\",\"depth\":1}";

        MvcResult result = mockMvc.perform(get("/api/sector/all/"))
                                  .andDo(print())
                                  .andExpect(status().isOk())
                                  .andExpect(content().string(containsString(expectedJson)))
                                  .andReturn();
    }

    @Test
    public void createUserForm() throws Exception {
        String payload = "{ \"agreementAccepted\": true, \"id\": 0, \"name\": \"string\", " +
                "\"sectors\": [{\"id\":2,\"name\":\"A\",\"depth\":1}]}";

        mockMvc.perform(post("/api/user-form/")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(payload))
               .andDo(print())
               .andExpect(status().isOk())
               .andExpect(jsonPath("$.id").value(1));
        ;
    }
}
