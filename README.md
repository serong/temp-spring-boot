# BACKEND

## USAGE
Run the spring boot app using local maven script

```shell script
./mvnw spring-boot:run 
```

Or using installed maven

```shell script
mvn spring-boot:run
```

Running the tests

```shell script
./mvnw test
```

## Database
H2 embedded database driver is used. Details can be found in 
`resources/application.properties`

H2 admin panel: http://localhost:8082/h2/

## Documentation
Swagger documentation for the API

Swagger: http://localhost:8082/swagger-ui.html

