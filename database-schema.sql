DROP TABLE IF EXISTS sector;
CREATE TABLE sector (
    id           INT PRIMARY KEY AUTO_INCREMENT,
    name         VARCHAR(100) NOT NULL,
    border_left  INT          NOT NULL,
    border_right INT          NOT NULL,
);

DROP TABLE IF EXISTS user_form;
CREATE TABLE user_form (
    id                 INT PRIMARY KEY AUTO_INCREMENT,
    name               VARCHAR(100) NOT NULL,
    agreement_accepted BOOLEAN      NOT NULL DEFAULT FALSE,
);

DROP TABLE IF EXISTS user_form_sectors;
CREATE TABLE user_form_sectors (
    user_form_id BIGINT NOT NULL,
    sectors_id   BIGINT NOT NULL,
);

ALTER TABLE user_form_sectors ADD CONSTRAINT fk_sectors_id FOREIGN KEY (sectors_id) REFERENCES sector;
ALTER TABLE user_form_sectors ADD CONSTRAINT fk_user_form_id FOREIGN KEY (user_form_id) REFERENCES user_form;
